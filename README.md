# Task 1.2: Drawing a square

Task Description:

Write source code for square.cpp that takes user input and draws an outline of a square using *.

User input should be a non-negative whole number. If they enter anything besides that – display a message and re-prompt the user.

An example of the output can be seen below, you should include such a screenshot within your code repository (Embedded it in README.md file).



## Sample

![](./sample_output.jpg)

Description: output sample square with 13x13 stars