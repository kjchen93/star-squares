#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>
#include <ctime>
using namespace std;

/*
Author: Kai Chen
 This code will generate a square with "*". User will input a positiv int and the program will output a sqaure made of stars.
*/

int main() {
    int squareSize; // Initialize the size of the square
    cout << "Enter number of row and columns for the square : ";
    cin >> squareSize; // user input

    while (!cin || squareSize <0) // check if the input is an int greater than 0
    {
        cout << "Sorry I need a positiv integer, try agiain : " << endl;
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
        cin >> squareSize;
    }

    for (int i = 0; i < squareSize; ++i){
        for (int j = 0; j < squareSize; ++j){
            if( (i == 0) || (i == squareSize-1)) {
            cout << "*"; // prints a row of stars in the first and last row
        }else if ((i != 0) && ((j==0)|| (j== squareSize-1))){
            cout << "*"; // first and last column of a middle row is a star
        }else{
            cout << " "; // spaces between first and last column of a middle row
        }
        
        }
        cout << "\n"; // new row
    }
}